#include <iostream>
#include "firework.h"
#include "drawgl.h"
#include <SFML/Graphics.hpp>
#include <list>
#include "params.h"

int main(){

    unsigned int width = 800 ;
    unsigned int height = 600 ;

    sf::RenderWindow app(sf::VideoMode(width , height, 32), "FIREWORK");

    app.setFramerateLimit(60);

    std::list<FireWorks> prsl;

    initGL(width,height);

    sf::Texture backgroundTexture;
    backgroundTexture.loadFromFile("background.png");
    sf::Sprite backgroundSprite;
    backgroundSprite.setTexture(backgroundTexture);
    backgroundSprite.setPosition(0,0);

    sf::Texture skyTexture;
    skyTexture.create(width,height);
    skyTexture.loadFromFile("sky.png");
    skyTexture.setRepeated(true);

    sf::Sprite skySprite(skyTexture);
    skySprite.setPosition(0,0);
    skySprite.setColor(sf::Color(255,255,255,50));


    sf::Shader parallaxShader;
    parallaxShader.loadFromMemory(
        "uniform float offset;"

        "void main() {"
        "    gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;"
        "    gl_TexCoord[0] = gl_TextureMatrix[0] * gl_MultiTexCoord0;"
        "    gl_TexCoord[0].x = gl_TexCoord[0].x + offset;"
        "    gl_FrontColor = gl_Color;"
        "}"
        , sf::Shader::Vertex);

    float offset = 0.f;

    sf::Clock clock;


    while (app.isOpen()){

        sf::Event event;
        while (app.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                app.close();
            if (event.type == sf::Event::Resized){
                auto size= app.getSize();
                initGL(size.x,size.y);
            }

            if(sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
                auto mousePos=sf::Mouse::getPosition(app);
                prsl.push_back(FireWorks(sf::Vector2f(mousePos.x,mousePos.y),
                                         randRange(Params::MIN_PARTICLES_NUM,
                                                   Params::MAX_PARTICLES_NUM)));
            }
        }

        app.pushGLStates();


        parallaxShader.setUniform("offset", offset += clock.restart().asSeconds() / 90);

        app.draw(backgroundSprite);
        app.draw(skySprite, &parallaxShader);

        app.popGLStates();

        drawPoints(prsl);

        app.display();
    }

    return 0 ;
}

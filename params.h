#ifndef PARAMS_H
#define PARAMS_H

#define M_PI 3.14159265358979323846264338327950288

namespace Params {
constexpr auto GRAVITY = 0.05f;
constexpr auto MIN_SPEED = 1.f;
constexpr auto MAX_SPEED = 10.f;
constexpr auto MIN_ANGLE = 0.f;
constexpr auto MAX_ANGLE = 2.f* static_cast<float>(M_PI);
constexpr auto MIN_COLOR = 0.f;
constexpr auto MAX_COLOR = 1.f;
constexpr auto MIN_POINT_SIZE = 1.f;
constexpr auto MAX_POINT_SIZE = 5.f;
constexpr auto MIN_SLOW = 0.97f;
constexpr auto MAX_SLOW = 0.99f;
constexpr auto ALPHA_STEP = 0.03f;
constexpr auto MIN_PARTICLES_NUM = 100.f;
constexpr auto MAX_PARTICLES_NUM = 500.f;
constexpr auto SCHLEIFE_SIZE = static_cast<int>((MAX_COLOR-MIN_COLOR)/ALPHA_STEP*0.5);
}
#endif // PARAMS_H

#include "firework.h"
#include <iostream>
#include <cmath>
#include <algorithm>
#include "params.h"
#include <SFML/OpenGL.hpp>

FireWorks::FireWorks(sf::Vector2f bPos, unsigned int size):
    particles(std::vector<sf::Vector2f>(size,bPos)),
    speed(std::vector<sf::Vector2f>(size)),
    color(randRange(Params::MIN_COLOR, Params::MAX_COLOR),randRange(Params::MIN_COLOR, Params::MAX_COLOR),randRange(Params::MIN_COLOR, Params::MAX_COLOR)),
    kGravity(Params::GRAVITY),
    pointSize (randRange(Params::MIN_POINT_SIZE,Params::MAX_POINT_SIZE)),
    alpha(Params::MAX_COLOR),
    slow(randRange(Params::MIN_SLOW, Params::MAX_SLOW)){

    std::generate(speed.begin(),speed.end(),
                  [](){auto z=randRange(Params::MIN_ANGLE,Params::MAX_ANGLE);
        return sf::Vector2f(cosf(z)*randRange(Params::MIN_SPEED,Params::MAX_SPEED),
                            sinf(z)*randRange(Params::MIN_SPEED,Params::MAX_SPEED));});
}

void FireWorks::Update(){

    auto i=0;
    for (auto &pos: particles) {
        pos.x+=speed[i].x;
        pos.y+=speed[i].y;
        speed[i]=speed[i]*slow;
        speed[i].y += kGravity;
        i++;
    }
    setParticlesPr();
    alpha-=Params::ALPHA_STEP;
}

void FireWorks::Draw(){

    glBegin(GL_POINTS);
    for(auto &pr:particles){
        glVertex2f(pr.x, pr.y);
    }
    glEnd();
}

void FireWorks::DrawSchleife(){

    glBegin(GL_POINTS);
    for(auto &pr:particles_pr)
        for(auto &p:pr)
        glVertex2f(p.x, p.y);
    glEnd();
}

std::vector<sf::Vector2f> FireWorks::Particles() const{
    return particles;
}

sf::Vector3f FireWorks::Color() const{
    return  color;
}

float FireWorks::PointSize() const{
    return pointSize;
}

float FireWorks::Alpha() const{
    return alpha;
}

void FireWorks::setParticlesPr(){
    if (!particles_pr.size()){
        particles_pr.resize(Params::SCHLEIFE_SIZE);
        for (auto &pr: particles_pr){
            pr.resize(particles.size());
            std::copy(particles.begin(),particles.end(),pr.begin());
            }
    }else{
        auto m =++particles_pr.begin();
        std::rotate(particles_pr.begin(),m,particles_pr.end());

        particles_pr[particles_pr.size()-1]=particles;
    }


}

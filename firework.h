#include <vector>
#include <SFML/Graphics.hpp>
#include "rand.h"

class FireWorks  {
    public:
       FireWorks()=delete;
       FireWorks(sf::Vector2f bPos, unsigned int size);
       void Update();
       void Init();
       void Draw();
       void DrawSchleife();
       std::vector<sf::Vector2f> Particles() const;
       sf::Vector3f Color() const;
       float PointSize() const;
       float Alpha() const;

private:
       void setParticlesPr();
       std::vector <sf::Vector2f> particles;
       std::vector <std::vector<sf::Vector2f>> particles_pr;
       std::vector <sf::Vector2f> speed;
       sf::Vector3f color;
       float kGravity;
       float pointSize;
       float alpha;
       float slow;
};

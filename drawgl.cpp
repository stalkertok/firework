#include "drawgl.h"


void initGL(unsigned int width,unsigned int height){

    glViewport(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(0,width, height, 0, 0, 1);

    glShadeModel(GL_SMOOTH);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    glDisable(GL_DEPTH_TEST);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glClearAccum(0.0f, 0.0f, 0.0f, 1.0f);

    glEnable(GL_POINT_SMOOTH);

    glEnable(GL_TEXTURE_2D);

}

#include "rand.h"

float randRange(float min, float max){
    return ( ((float)rand() / (float)RAND_MAX) * (max - min)) + min;
}

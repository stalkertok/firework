#ifndef DRAWGL_H
#define DRAWGL_H

#include <SFML/OpenGL.hpp>
#include <SFML/Graphics.hpp>
#include <list>

void initGL(unsigned int width,unsigned int height);

template <typename T>

void drawPoints(T &poinst){

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();


    for (auto &prs: poinst){
        prs.Update();
        auto color =prs.Color();
        glColor4f(color.x, color.y, color.z, prs.Alpha());
        glPointSize(prs.PointSize());

        prs.Draw();
        prs.DrawSchleife();

    }

    poinst.remove_if([](const auto &prs){ return prs.Alpha() <= 0; });

};

#endif // DRAWGL_H
